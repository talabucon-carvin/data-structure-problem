// Simulation of a JSON returned from API Calls from a NoSQL database

//nested arrays and objects


let userData = [
	{	
	userId: 91827465,
	name: "admin",
	email: "nimda@mail.com",
	mobile: 09876543210,
	isAdmin: true,
	enrollments: []
	},
	{
	userId: 56473829,
	name: "Carvin",
	email: "talabucon@gmail.com",
	mobile: 09283547512,
	isAdmin: false,
	enrollments: [{
		courseId: "p-121",
		dateEnrolled: "12-01-20"
	},
	{
		courseId: "p-131",
		dateEnrolled: "01-01-21"

	},
	{
		courseId: "p-141",
		dateEnrolled: "02-01-21"
	}]
	},
	{
	userId: 475638291,
	name: "Dood",
	email: "arch@gmail.com",
	mobile: 09123456789,
	isAdmin: false,
	enrollments: [{
		courseId: "p-101",
		dateEnrolled: "12-01-20"
	},
	{
		courseId: "p-131",
		dateEnrolled: "01-01-21"

	},
	{
		courseId: "p-141",
		dateEnrolled: "02-01-21"

	}


		]
	}
]

let courseData= [
	{
		courseId: "p-101",
		name: "HTML",
		price: 5000,
		description: "lorem",
		enrollees: [{
			userId: 475638291,
			dateEnrolled: "12-01-20"
		}
		]
	},
	{
		courseId: "p-121",
		name: "CSS",
		price: 6000,
		description: "ipsum",
		enrollees: [{
			userId: 56473829,
			dateEnrolled: "12-01-20"
		}]
	},
	{
		courseId: "p-131",
		name: "Javascript",
		price: 7000,
		description: "dolor",
		enrollees: [{
			userId: 56473829,
			dateEnrolled: "01-01-21"
		},
		{
			userId: 475638291,
			dateEnrolled: "01-01-21"
		}
		]
	},
	{
		courseId: "p-141",
		name: "Bootstrap",
		price: 5000,
		description: "sit",
		enrollees: [{
			userId: 56473829,
			dateEnrolled: "02-01-21"
		},
		{
			userId: 475638291,
			dateEnrolled: "02-01-21"
		}
		]
	},		

]

// console.log(userData)
// console.log(courseData)

/* Make a function that accepts a course name argument and returns an array of the enrollees' name*/
let findCourseEnrollees = (courseName) => {

	let enrolleesArr = []
	// iterate through coursedata to find one with argument's name
	for (let i=0; i < courseData.length; i++) {
		// console.log(courseData[i].name)

		//match argument to course data and if equal
		if (courseName === courseData[i].name) {
			// console.log(`${courseData[i].name} found`)

			//iterate through enrollees of the current course
			for (let j=0; j < courseData[i].enrollees.length; j++) {
				// console.log(courseData[i].enrollees)

				//iterate through userData 
				for (let k=0; k < userData.length; k++) {
					// console.log(userData[k].name)
					// console.log(courseData[i].enrollees[j].userId)

					//if coursedata's enrollee's userid match with userdata's user's userid then we found the course's enrollees. 
					if (courseData[i].enrollees[j].userId === userData[k].userId) {
						// console.log(userData[k].name)
						enrolleesArr.push(userData[k].name)
						
					}
				}
			}
		}
	}
	let format = "";
	let format2;

	// Simulated frontend output

	if (enrolleesArr.length === 0) {
		format2 = `No current Enrollees for the course ${courseName}`
		console.log(format2)
		return false;

	} else if (enrolleesArr.length === 1) {
		
		format2 = `${enrolleesArr} is enrolled on ${courseName}`
		console.log(format2)

	} else if (enrolleesArr.length > 1) {

		for (l=0; l < enrolleesArr.length - 1; l++) {
			format += enrolleesArr[l] + ", "
			format2 = format.slice(0,-2)
		}
		format2 += " and " + enrolleesArr[enrolleesArr.length - 1] + ` are enrolled in ${courseName}`
		console.log(format2)


	}
	console.log(enrolleesArr)
	return enrolleesArr
}


findCourseEnrollees('HTML');
findCourseEnrollees('CSS')
findCourseEnrollees('Javascript')
findCourseEnrollees('Bootstrap')